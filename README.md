Prerequisites:
Tensorflow

How to install:
 - clone this repo
 - run desired python script
 
For image recognition:
 - python train.py (number of iterations can be changed)
 - python predict.py [filename]
 
output gives [p_bowl p_vase] which are the probability prediction for respectively bowl and vase

For generating new models:
 - python generate_models.py
 
path to .bom file can be changed

writes new model to ./generated/

For generating outline .png files from a .stl file which can be used as inputs for the image recognition:
 - python outline_stl.py
 
path to .stl file can be changed

writes 3 .png files to ./outlines/