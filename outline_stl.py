# Created by Sijmen van der Willik
# 11-12-17 15:22

import numpy as np
from stl import mesh
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
import scipy.misc

# takes an .stl file as input, calculates an outline through the weighted center of the model by projecting the points
#  that are furthest away from the center on the plane that goes through the center

# set boolean to True to display plot
show_plot = False

# Load the STL files and add the vectors to the plot
your_mesh = mesh.Mesh.from_file('/home/sijmen/learning/bowl-vase-img-recognition/stl/vase/0a61fcb4-15cd-4888-a200-d9aafc46f749.stl')

# get point cloud from meshes and calculate center of meshes
point_cloud = your_mesh.points
weighted_point_cloud = np.ndarray([np.shape(point_cloud)[0], 3])

for i in range(np.shape(point_cloud)[0]):
    x = point_cloud[i]
    weighted_point_cloud[i] = [(x[0]+x[3]+x[6])/3, (x[1]+x[4]+x[7])/3, (x[2]+x[5]+x[8])/3]

# set resolution for result image
resolution = [120, 120]


def get_step_n(value, min_max, n_steps, step_size=None):
    """Calculates the step for the input value for given parameters

    :param value: <float> the value for which the step number must be determined
    :param min_max: <array of size 2> min and max values of the range
    :param n_steps: <int> total number of steps
    :param step_size: <float> overrides calculated step size if given
    :return:
    """

    value -= min_max[0]
    distance = abs(min_max[0] - min_max[1])
    if step_size is None:
        step_size = distance / n_steps

    result_step = int(value / step_size)

    if result_step == n_steps:
        result_step -= 1

    return result_step


def generate_outline(input_pc, res):
    """Generates a res[0] by res[1] matrix of a point cloud representing a black and white image

    each cell represents one pixel

    sorts array for speed

    image floats to top left

    :param input_pc: <2d-array with shape <n, 2>> point cloud
    :param res: <int array of size 2> resolution of the generated image
    :return: <2d-numpy array> generated image
    """
    input_pc = input_pc[input_pc[:, 0].argsort()]

    # init result image
    res_image = np.ndarray([res[0], res[1]])
    res_image.fill(0)

    # calculate range and step values
    range_0 = [np.min(input_pc[:, 0]), np.max(input_pc[:, 0])]
    range_1 = [np.min(input_pc[:, 1]), np.max(input_pc[:, 1])]
    step_sizes = [abs(range_0[0]-range_0[1])/res[0], abs(range_1[0]-range_1[1])/res[1]]
    max_step_size = max(step_sizes)

    # init step variables
    step_0 = 0
    cur_min = 999999999
    cur_max = -999999999

    # loop over rows, calculate step and find min and max on other axis for this step and put 1 in result image for
    #  these values
    for row in input_pc:
        tmp_step = get_step_n(row[0], range_0, res[0])

        if tmp_step > step_0:
            # if new step, calc and add old step to image matrix, then reset values
            step_1_n_min = get_step_n(cur_min, range_1, res[1], step_size=max_step_size)
            step_1_n_max = get_step_n(cur_max, range_1, res[1], step_size=max_step_size)

            res_image[step_0][step_1_n_min] = 1
            res_image[step_0][step_1_n_max] = 1

            step_0 = tmp_step
            cur_min = 999999999
            cur_max = -999999999

        if row[1] < cur_min:
            cur_min = row[1]

        if row[1] > cur_max:
            cur_max = row[1]

    return res_image

# save outline of all 3 views
folder = "./outlines/"

outline_array = generate_outline(weighted_point_cloud[:, 0:2], resolution)
scipy.misc.toimage(outline_array, cmin=0.0, cmax=1.0).save(folder + 'outline01.png')

outline_array = generate_outline(weighted_point_cloud[:, 1:3], resolution)
scipy.misc.toimage(outline_array, cmin=0.0, cmax=1.0).save(folder + 'outline12.png')

outline_array = generate_outline(weighted_point_cloud[:, [0, 2]], resolution)
scipy.misc.toimage(outline_array, cmin=0.0, cmax=1.0).save(folder + 'outline02.png')

if show_plot:
    fig = pyplot.figure()
    ax = Axes3D(fig)
    ax.scatter(weighted_point_cloud[:, 0], weighted_point_cloud[:, 1], weighted_point_cloud[:, 2])
    ax.set_xlim(-500, 500)
    ax.set_ylim(-500, 500)
    ax.set_zlim(-500, 500)
    pyplot.show()
