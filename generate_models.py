# Created by Sijmen van der Willik
# 8-12-17 15:02

import os

import json
import random
import time
import datetime

import shape_generator
import predict

bom_file = './bom/vase/0a25a943-b401-45b9-aa2b-690aca631f73.bom'
tmp_folder = "tmp/"

# read .bom file as JSON
start_obj = json.load(open(bom_file))


# do depth-first search for variables with known constraints and adjust them


def adjust_known_constraints(variable):
    """Does the actual adjusting of the values from a .bom file variable

    :param variable: <dict, list> variable which holds constraints
    :return:
    """

    # constraints seem to either contain a list of dicts or a min/max/step description for the value
    try:
        # this adjusts values for which a dict with options was found
        no_options = len(variable['constraints']['values'])

        options = []

        for i in range(no_options):
            options.append(variable['constraints']['values'][i]['value'])

        print "options:", options

        # pick random from options and overwrite if different
        variable['value'] = random.choice(options)

    except:
        # this adjusts values for which a min/max/step descriptor was found
        min_v = variable['constraints']['min']
        max_v = variable['constraints']['max']
        step_v = variable['constraints']['step']
        variable['value'] = step_v * round(random.uniform(min_v, max_v)/step_v)


def mutate_bom(input_dict):
    """Mutates a .bom file by finding variables with known constraints and adjusts them randomly and uniformly within
     these constraints

     this is a recursive function

    :param input_dict: <dict> variable where to search for constraints
    :return:
    """
    try:
        _ = input_dict['constraints']
        adjust_known_constraints(input_dict)
    except:
        if isinstance(input_dict, dict):
            for key in input_dict:
                mutate_bom(input_dict[key])

# find constraints and adjust them
mutate_bom(start_obj)

# print new .bom file
out_name = datetime.datetime.fromtimestamp(time.time()).strftime('%Y_%m_%d_%H_%M_%S') + ".bom"

with open(tmp_folder + out_name, 'w') as outfile:
    json.dump(start_obj, outfile)

# use api to get .png (and .stl), also calculate original for comparison
shape_generator.call_api(tmp_folder + out_name, out_path=tmp_folder)
shape_generator.call_api(bom_file, out_path=tmp_folder)

# place in folder as predict classifies it
png_name = tmp_folder + out_name.split(".")[0] + ".png"
prediction = predict.predict(png_name)

if prediction[0][0] > prediction[0][1]:
    object_type = "bowl"
else:
    object_type = "vase"

os.rename(png_name, "generated/" + object_type + "/" + png_name.split("/")[-1])
