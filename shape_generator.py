#!/usr/bin/python
import os
import sys

'''
usage: python[3] shape_generator.py <filename> <outputpath> (local explicit path to .bom file)
'''


def call_api(filename, out_path="./"):
    head, tail = os.path.split(filename)
    root, _ = os.path.splitext(tail)

    # generate a thumbnail of the model via the remote api
    cmd = "curl -ko \"" + out_path + root + ".png\" -H \"Content-Type: application/json\" -X POST -d @" + filename + "  https://dev.borges.xyz/api/thumbnail/?size=256"
    os.system(cmd)

    # generate an stl file of the model via the remote api
    cmd = "curl -ko \"" + out_path + root + ".stl\" -H \"Content-Type: application/json\" -X POST -d @" + filename + "  https://dev.borges.xyz/object/stl/"
    os.system(cmd)


if __name__ == "__main__":
    filename = sys.argv[1]

    if len(sys.argv) > 2:
        out_path = sys.argv[2]
        call_api(filename, out_path=out_path)
    else:
        call_api(filename)
